# Templates for jupyter notebooks

This is a collection of thematic jupyter notebook templates to be used on this workbench. 

#### [Project homepage](https://eredekop@bitbucket.org/eredekop/nbtemplates.git)


### Requirements 

### The directory structure
------------

The directory structure of the project looks like this: 

```
├── README.md
├── xps_notebook.ipynb              <- XPS-related notebooks 
└── tap_notebook.ipynb              <- TAP-related notebooks
```

### Using the templates
------------


    cp template_path project_path/notebooks

